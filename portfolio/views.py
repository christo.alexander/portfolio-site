from __future__ import unicode_literals

from django.views.generic import ListView
from django.views.generic.edit import FormView
from django.contrib.messages.views import SuccessMessageMixin

from django.core.mail import send_mail, get_connection
from django.conf import settings

from .models import Project

class ProjectListView(ListView):
    model = Project # data from database
    template_name = 'portfolio/index.html'
    # context_object_name = 'projects' # name of the var in html template
    queryset = Project.objects.all()#  list of all projects
    object_list = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["projects"] = Project.objects.all()
        return context

